# Introduction

This software transforms an arbitrarily deeply nested HTML list into Miller
Columns.

# Setup

1. Clone the repository into a web directory.
1. Point the browser to `miller-columns/index.html`.

# Example

Include the following files:

```html
<link type="text/css" rel="stylesheet" href="theme.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="columns.min.js"></script>
```

Implement the following script:

```javascript
$(document).ready( function() {
  $("div.columns").millerColumns({
    current: function( $item ) {
      console.log( "User selected: " + $item.text() );
    }
  });
});
```

The `current` function is called when the user selects an item. The `$item`
parameter is the item that the user selected. If the user presses the Escape
key (or otherwise deselects all items), then `$item` will be `undefined`.

Create nested HTML elements as follows:

```html
<div id="container">
  <div class="breadcrumb"></div>
  <div class="columns" tabindex="1">
    <ul>
      <li>Level 1</li>
      <li>Level 1
        <ul>
          <li>Level 2</li>
          <li>Level 2
            <ul>
              <li>Level 3</li>
              <li>Level 3</li>
            </ul>
          </li>
          <li>Level 2</li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="toolbar"></div>
</div>
```

The `breadcrumb`, `toolbar`, and `container` items are optional. The `tabindex`
is necessary for keyboard interaction. See `index.html` for additional details.

