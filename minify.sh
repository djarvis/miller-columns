#!/bin/bash

# Path to Closure Compiler
COMPILER=$HOME/archive/closure-javascript.jar

# Java must be in the PATH
COMMAND="java -jar $COMPILER"

FILE=$1

# Allow overriding default filename from the command line.
if [ -z $FILE ]; then
  FILE="columns.js"
fi

MINIFY="$COMMAND --js=$FILE --js_output_file=$(basename $FILE .js).min.js"

# Execute the command
$MINIFY

